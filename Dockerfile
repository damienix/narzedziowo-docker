FROM node:7

RUN wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add -
RUN echo deb http://dl.google.com/linux/chrome/deb/ stable main >> /etc/apt/sources.list.d/google.list
RUN echo deb http://http.debian.net/debian jessie-backports main >> /etc/apt/sources.list
RUN apt-get update -qy
RUN apt-get install -y ruby ruby-dev openjdk-8-jre firefox-esr google-chrome-stable xvfb
RUN gem install dpl

